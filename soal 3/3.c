#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>

//3a
int main(void){
  pid_t child_id, child_id1, child_id2;
  int status;

  child_id = fork();

  if(child_id < 0){
    exit(EXIT_FAILURE); //Program akan berhenti, jika gagal membuat proses baru
    }

  if(child_id == 0){
    char *argv[] = {"mkdir", "-p", "darat", NULL};
    execv("/bin/mkdir", argv);
  } else {
    while ((wait(&status)) > 0);
    child_id1 = fork();

    if(child_id1 < 0){ 
      exit(EXIT_FAILURE); //Program akan berhenti, jika gagal membuat proses baru
    }

    if(child_id1 == 0){
      sleep(3);
      char *argv[] = {"mkdir", "-p", "air", NULL};
      execv("/bin/mkdir", argv);
    }
   //3b    
   else
    {
      while ((wait(&status)) > 0);
      child_id2 = fork();
      if (child_id2 == 0)
      {
        char *argv[] = {"unzip", "animal.zip", NULL};
        execv("/bin/unzip", argv);
      }
  }
  }
}
