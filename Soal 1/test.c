#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <wait.h>
#include <stdio.h>
#include <dirent.h>
#include <time.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>
#include <json-c/json.h>

// a. install file from link

typedef struct _object{
    char name[50];
    int rarity;
}object;

void zip_download(){
    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if (child_id == 0) {
        // this is child
        char *argv[] = {"wget", "-q", "https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download", "-O", "Anggap_ini_database_characters.zip", NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    } else {
        // this is parent
        while ((wait(&status)) > 0);

        char *argv[] = {"wget", "-q", "https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download", "-O", "Anggap_ini_database_weapon.zip", NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    }
}

void unzip(){
    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if (child_id == 0) {
    // this is child

        char* argv[] = {"unzip", "-q","-n","Anggap_ini_database_characters.zip", "-d", "gacha_gacha", NULL};
        execv("/bin/unzip",argv);
        exit(0);

    } else {
    // this is parent
        while ((wait(&status)) > 0);
        char* argv[] = {"unzip", "-q","-n","Anggap_ini_database_weapon.zip", "-d", "gacha_gacha", NULL};
        execv("/bin/unzip",argv);
        exit(0);
    }
}

void mkdir_gachagacha(){
    char *argv[] = {"mkdir", "-p", "gacha_gacha", NULL};
    execv("/bin/mkdir", argv);
    exit(0);
}

void mkdir_custom(char* dir){
    char *argv[] = {"mkdir", "-p", dir, NULL};
    execv("/bin/mkdir", argv);
    exit(0);
}

int json_count(char *path){
    DIR *dp;
    struct dirent *ep;
    int count = 0;

    dp = opendir(path);

    if (dp != NULL)
    {
      while ((ep = readdir (dp)))if(strstr(ep->d_name, ".json") != NULL) count++;

      (void) closedir (dp);
    } else perror ("Couldn't open the directory");
    return count;
}

void object_get(object *temp, char* path){
    FILE *fptr;
    char buffer[5000];

    struct json_object *parsed_json, *name, *rarity;
    fptr = fopen(path, "r");
	fread(buffer, 5000, 1, fptr);
	fclose(fptr);

	parsed_json = json_tokener_parse(buffer);

	json_object_object_get_ex(parsed_json, "name", &name);
	json_object_object_get_ex(parsed_json, "rarity", &rarity);

	strcpy(temp->name, json_object_get_string(name));
	temp->rarity = json_object_get_int(rarity);
}

void gacha_get(object* temp, char* dir){
    DIR *dp;
    struct dirent *ep;
    char path[500];
    int index = 0;

    dp = opendir(dir);

    if (dp != NULL)
    {
      while ((ep = readdir (dp)))if(strstr(ep->d_name, ".json") != NULL) {
          sprintf(path, "%s/%s", dir, ep->d_name);
          object_get(&temp[index++], path);
      }

      (void) closedir (dp);
    } else perror ("Couldn't open the directory");
}

// b. output hasil gacha di file.txt
// c. format penamaan file
// d. buying gacha item dengan primogem
// e. nambah file dan delete setelah 3 jam

void execute(){
    pid_t child;
    int status;

    child = fork();

    if(child < 0) exit(EXIT_FAILURE);

    if(child == 0){
        zip_download();
    }

    else{

        while ((wait(&status)) > 0);
        child = fork();
        if(child < 0) exit(EXIT_FAILURE);

        if(child == 0){
            mkdir_gachagacha();
        }
        else{
            while ((wait(&status)) > 0);
            
            child = fork();
            if (child < 0) {
                exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
            }

            if (child == 0) {
                // this is child
                unzip();

            } else {
                // this is parent
                while ((wait(&status)) > 0);

                if ((chdir("gacha_gacha")) < 0)exit(EXIT_FAILURE);

                object *characters, *weapons, temp;
                FILE *fptr;
                struct tm* local;
                time_t t;
                int primogem = 79000, counter = 0;
                int char_count = json_count("characters"), weapon_count = json_count("weapons");
                char dirname[20], filename[200], objname[200];
                characters = (object*)malloc(sizeof(object)*char_count);
                weapons = (object*)malloc(sizeof(object)*weapon_count);

                gacha_get(characters, "characters");
                gacha_get(weapons, "weapons");

                while(primogem >= 160){
                    if(counter%90 == 0){
                        child = fork();
                        if (child < 0)exit(EXIT_FAILURE);

                        sprintf(dirname, "total_gacha_%d", counter+90);
                        
                        if (child == 0) {
                            mkdir_custom(dirname);
                            exit(0);
                        } else while ((wait(&status)) > 0);
                    }
                    if(counter%10 == 0){
                        if(counter) fclose(fptr);
                        sleep(1);
                        t = time(NULL);
                        local = localtime(&t);
                        sprintf(filename, "%s/%02d:%02d:%02d_gacha_%d.txt", dirname, local->tm_hour, local->tm_min, local->tm_sec, counter+10);
                        fptr = fopen(filename, "w");
                    }
                    counter++;
                    primogem-=160;
                    if(counter&1){
                        temp = characters[rand()%char_count];
                        sprintf(objname, "%d_characters_%s_%d_%d", counter, temp.name, temp.rarity, primogem);
                    }
                    else {
                        temp = weapons[rand()%weapon_count];
                        sprintf(objname, "%d_weapons_%s_%d_%d", counter, temp.name, temp.rarity, primogem);
                    }
                    fprintf(fptr, "%s\n", objname);
                }
                fclose(fptr);
            }
        }
    }
}

void gacha_zip(){
    char* argv[] = {"zip",
                    "-P",
                    "satuduatiga", 
                    "-r", 
                    "../not_safe_for_wibu.zip", 
                    "total_gacha_90",  
                    "total_gacha_180",  
                    "total_gacha_270",  
                    "total_gacha_360",  
                    "total_gacha_450",  
                    "total_gacha_540", 
                    NULL};
    execv("/bin/zip",argv);
    exit(0);
}

void zip_and_delete(){
    pid_t child;
    int status;
    system("ls");

    child = fork();
    if (child < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if (child == 0) {
        // this is child
        gacha_zip();
    } else {
        // this is parent
        while ((wait(&status)) > 0);
        if ((chdir("..")) < 0)exit(EXIT_FAILURE);
        char *argv[] = {"rm", "-r", "gacha_gacha", NULL};
        execv("/bin/rm", argv);
        exit(0);
    }
}

int main(){
    srand(time(0));
    pid_t pid, sid;        // Variabel untuk menyimpan PID
    struct tm* local;
    time_t t;

    pid = fork();     // Menyimpan PID dari Child Process

    /* Keluar saat fork gagal
    /* (nilai variabel pid < 0) */
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    /* Keluar saat fork berhasil
    /* (nilai variabel pid adalah PID dari child process) */
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while (1) {
        t = time(NULL);
        local = localtime(&t);
        if(local->tm_mday == 30 &&
           local->tm_mon == 2 &&
           local->tm_hour == 4 &&
           local->tm_min == 44) execute();
        else if(local->tm_mday == 30 &&
                local->tm_mon == 2 &&
                local->tm_hour == 7 &&
                local->tm_min == 44) zip_and_delete();

        sleep(30);
    }
    

    return 0;
}
