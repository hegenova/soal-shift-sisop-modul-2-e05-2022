#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

void make_dir(char* name){
    char *argv[] = {"mkdir", "-p", name, NULL};
    execv("/bin/mkdir", argv);
    exit(0);
}

void download(){
    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        char *argv[] = {"wget", "--no-check-certificate", "'https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp'", "-O", "Characters", NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    } 
    else {
        while ((wait(&status)) > 0);
        char *argv[] = {"wget", "--no-check-certificate", "'https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT'", "-O", "Weapons", NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    }
}

void unzip(){
    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        char *argv[] = {"unzip", "Characters.zip", "-d", "gacha_gacha", NULL};
        execv("/bin/unzip", argv);
        exit(0);
    } 
    else {
        while ((wait(&status)) > 0);
        char *argv[] = {"unzip", "Weapons.zip", "-d", "gacha_gacha", NULL};
        execv("/bin/unzip", argv);   
        exit(0);
    }
}

int main() {
    char dir_name[30];
    sprintf(dir_name, "gacha_gacha");
    make_dir(dir_name);
}

//gcc -o ~/Documents/VsCode/SISOP/Gitlab/soal-shift-sisop-modul-2-e05-2022-main/Soal\ 1/main.c
//gcc ~/Documents/GitHub/soal-shift-sisop-modul-2-e05-2022/Soal\ 1/main.c